from rest_framework import serializers
from .models import *

class EventoSerializer(serializers.ModelSerializer):
    title = serializers.CharField(source='titulo')
    start = serializers.DateTimeField(source='inicio')
    end = serializers.DateTimeField(source='fin')

    class Meta:
        model = Evento
        fields = [
            'id',
            'title',
            'start',
            'end'
        ]
        
        
class ContactoSerializer(serializers.ModelSerializer):
    class Meta:
        model = Contacto
