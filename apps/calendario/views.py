from django.shortcuts import render
from django.views.generic import TemplateView
from rest_framework import viewsets
from .models import *
from .serializers import *

class EventoViewSet(viewsets.ModelViewSet):
    serializer_class = EventoSerializer
    queryset = Evento.objects.all()
    
    
class ContactoViewSet(viewsets.ModelViewSet):
    serializer_class = ContactoSerializer
    queryset = Contacto.objects.all()


class CalendarioView(TemplateView):
    template_name = "calendario.html"
