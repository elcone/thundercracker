from django.conf.urls import url, include
from rest_framework import routers
from .views import *

router = routers.DefaultRouter()
router.register(r'eventos', EventoViewSet)
router.register(r'contactos', ContactoViewSet)

urlpatterns = [
    url(r'api/', include(router.urls)),
    url(r'^$', CalendarioView.as_view(), name='calendario')
]
