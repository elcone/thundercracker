from django.contrib import admin
from .models import *

@admin.register(Evento)
class EventoAdmin(admin.ModelAdmin):
    list_display = [
        'titulo',
        'inicio',
        'fin'
    ]
    
    
@admin.register(Contacto)
class ContactoAdmin(admin.ModelAdmin):
    pass
