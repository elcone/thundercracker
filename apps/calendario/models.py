from django.db import models

class Contacto(models.Model):
    nombre = models.CharField(max_length=30)
    apellido_paterno = models.CharField(max_length=30, null=True, blank=True)
    apellido_materno = models.CharField(max_length=30, null=True, blank=True)
    
    def __str__(self):
        return self.nombre


class Evento(models.Model):
    titulo = models.CharField(max_length=100)
    inicio = models.DateTimeField()
    fin = models.DateTimeField()
    contacto = models.ForeignKey(Contacto, related_name='eventos', null=True)

    def __str__(self):
        return self.titulo
