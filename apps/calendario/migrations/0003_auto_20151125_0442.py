# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('calendario', '0002_auto_20151122_1844'),
    ]

    operations = [
        migrations.AlterField(
            model_name='contacto',
            name='apellido_materno',
            field=models.CharField(null=True, blank=True, max_length=30),
        ),
        migrations.AlterField(
            model_name='contacto',
            name='apellido_paterno',
            field=models.CharField(null=True, blank=True, max_length=30),
        ),
    ]
